<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


global $bbp_hft_license_status_info;

function bbp_hft_admin_menu_item() {
	add_submenu_page( null, 'J7Digital Plugin Updater License', 'J7Digital Plugin Updater License', 'manage_options', 'bbp_hft_license', 'bbp_hft_license_page' );
}
add_action('admin_menu', 'bbp_hft_admin_menu_item');

function bbp_hft_license_page() {

$license 	= get_option( 'bbp_hft_license_key' );
$status 	= get_option( 'bbp_hft_license_status' );
$bbp_hft_license_status_info = get_option( 'bbp_hft_license_status_info' );

	?>

	<script>
		(function($){
			jQuery(document).ready(function(){

				var key = jQuery('#license_key').val();

				if ( key == '')
				{
					jQuery('#activation_button').attr('disabled',true);
					jQuery('#activation_button').val('Save License Below');
				}
				else
				{
					jQuery('.prompt_message').css('color','orange');
				}

				jQuery('#license_key').keyup(function() {
					apsppScriptFunction();
				});

				jQuery('#license_key').focusout(function() {
					apsppScriptFunction();
				});

				function apsppScriptFunction(){
					var check = jQuery('#license_key').val();
					if (  check != key ){
						jQuery('#activation_button').attr('disabled',true);
						jQuery('#activation_button').val('Save New License Below');

					} else{
						jQuery('#activation_button').removeAttr('disabled');
						jQuery('#activation_button').val('Activate License');
					}
				}
			});
		})(jQuery);
	</script>

	<div class="wrap">
		<h1><?php _e('J7Digital Plugin Updater License'); ?></h1>
		<hr/>
		<form method="post" action="options.php">
			<?php settings_fields('bbp_hft_license_fields');
			if( $license == false or $status !=='valid' or $status =='false') { ?>
				<h3><?php _e('License — <span style="color: red"><em class="prompt_message">Not Yet Saved!</em></span>'); ?></h3>
				<?php
				if ($bbp_hft_license_status_info != 'Server Error' && $bbp_hft_license_status_info != NULL && $bbp_hft_license_status_info->success == false){
					echo '<p>Reason Code: ' . $bbp_hft_license_status_info->error . '</p>';
					echo '<p>License Limit: ' . $bbp_hft_license_status_info->license_limit . '</p>';
					echo '<p>Activations Left: ' . $bbp_hft_license_status_info->activations_left . '</p>';
					delete_option('bbp_hft_license_status_info');
				}
				elseif($bbp_hft_license_status_info == 'Server Error')
				{
					echo '<p>Error: '.$bbp_hft_license_status_info .'</p>';
					delete_option('bbp_hft_license_status_info');
				}
				else
				{
					delete_option('bbp_hft_license_status_info');
				}
				?>
				<h3><?php _e('License Key'); ?></h3>
				<input id="license_key" name="bbp_hft_license_key" type="text" class="regular-text" value="<?php esc_attr_e( $license ); ?>" />

				<?php wp_nonce_field( 'bbp_hft_nonce', 'bbp_hft_nonce' ); ?>

				<input type="submit" id="activation_button" class="button-secondary" name="bbp_hft_license_activater" value="<?php _e('Activate License'); ?>"/>

				<hr/>
				<?php submit_button(); ?>
				<?php } else { ?>
					<h1 style="padding: 80px 80px; margin-top: 100px; background-color: green; color: white;"> <?php _e('Plugin is Activated! Thank you and enjoy!'); ?> </h1>

					<?php } ?>
				</form>
			</div>
			<?php }


			function bbp_hft_register_option() {
				register_setting('bbp_hft_license_fields', 'bbp_hft_license_key', 'bbp_hft_sanitize_license' );
			}
			add_action('admin_init', 'bbp_hft_register_option');

			function bbp_hft_sanitize_license( $new ) {
				$old = get_option( 'bbp_hft_license_key' );
				if( $old && $old != $new ) {
		delete_option( 'bbp_hft_license_status' ); // new license has been entered, so must reactivate
	}
	return $new;
}

function bbp_hft_activate_license() {
	if( isset( $_POST['bbp_hft_license_activater'] ) ) {

		if( ! check_admin_referer( 'bbp_hft_nonce', 'bbp_hft_nonce' ) )
		{
			return;
		}

		$license = trim( get_option( 'bbp_hft_license_key' ) );
		$api_params = array(
			'edd_action'=> 'activate_license',
			'license' 	=> $license,
			'item_name' => urlencode( bbp_hft_item_name ), // the name of our product in EDD
			'url'       => home_url()
			);

		$response = wp_remote_post( bbp_hft_store_url, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

		if ( is_wp_error( $response ) )
		{
			return false;
			update_option( 'bbp_hft_license_status_info','Server Error');
		}
		else
		{
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );
			if($license_data)
			{
			update_option( 'bbp_hft_license_status_info', $license_data);
			update_option( 'bbp_hft_license_status_authsite', home_url());
			update_option( 'bbp_hft_license_status', $license_data->license );
			}
			else
			{
				update_option( 'bbp_hft_license_status_info','Server Error');

			}
		}
	}

}

add_action( 'admin_init', 'bbp_hft_activate_license' );


function bbp_hft_sync_license() {

		$license = trim( get_option( 'bbp_hft_license_key' ) );

		// Have License
		if ( $license) {

		$api_params = array(
			'edd_action'=> 'check_license',
			'license' 	=> $license,
			'item_name' => urlencode( bbp_hft_item_name ), // the name of our product in EDD
			'url'       => urlencode( home_url() )
			);


		$response = wp_remote_get( esc_url_raw(add_query_arg ( $api_params, bbp_hft_store_url)), array( 'timeout' => 15, 'sslverify' => false) );

		if ( ! is_wp_error( $response ) ) {

		$license_data = json_decode( wp_remote_retrieve_body( $response ) );
		return $license_data;
}

}
}




function bbp_hft_license_sync_periodic() {


	// Admin only
	if ( ! is_admin() ) {
		return;
	}


	//Periodically in relevant areas or always on Theme License page
	$screen = get_current_screen();

	// Has this been checked in last day or is it theme license page?
	if ( ! get_transient( 'bbp_hft_license_sync_periodic' ) || $screen->base == 'settings_page_bbp_hft_license') {

		// Check remote status
		$license_data = bbp_hft_sync_license();
		// Set transient to prevent check until next week
		set_transient( 'bbp_hft_license_sync_periodic', true, WEEK_IN_SECONDS );

		if ($license_data && ($license_data->license == 'invalid' OR $license_data->license == 'expired' OR $license_data->license == 'disabled' OR $license_data->license == 'site_inactive')){
		delete_option( 'bbp_hft_license_status_info');
		delete_option( 'bbp_hft_license_status_authsite');
		delete_option( 'bbp_hft_license_status');
	}

}

}

add_action( 'current_screen', 'bbp_hft_license_sync_periodic' );

